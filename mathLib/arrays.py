import math


def size(array: list) -> int:
    sum = 0

    for _ in array:
        sum += 1
    return sum

def is_empty(array: list) -> None:
    if not size(array):
        return True
    return False


def max(array: list) -> float:
    if is_empty(array):
        raise ValueError("Array is empty")

    max_value: float = array[0]
    for val in array:
        if val > max_value:
            max_value = val
    return max_value

def min(array: list) -> float:
    if is_empty(array):
        raise ValueError("Array is empty")

    min_value: float = array[0]
    for val in array:
        if val < min_value:
            min_value = val
    return min_value

def average(array: list) -> float:
    if is_empty(array):
        raise ValueError("Array is empty")

    sum: float = 0
    for val in array:
        sum += val
    return sum / size(array)

def std_dev(array: list) -> float:
    len_array = size(array)
    mean: float = average(array)
    deviations = [(value - mean)**2 for value in array]
    variance = sum(deviations) / len_array
    return math.sqrt(variance)

def reverse(array: list) -> list:
    return [array[x] for x in range(len(array)-1, -1, -1)]

def sort(array: list, type: str = 'asc') -> list:
    len_array = size(array)
    for i in range(len_array):
        for j in range(len_array-i-1):
            if array[j] > array[j+1]:
                array[j], array[j+1] = array[j+1], array[j]

    if type == 'desc':
        return reverse(array)
    
    return array
