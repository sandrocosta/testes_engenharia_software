import unittest

from mathLib import arrays

class TestArray(unittest.TestCase):
    
    def setUp(self):
        self.sample_array = [7,5,4,9,12,45]
      
    def test_max_correct_value_from_array_with_different_values(self):
        """
        Test the max value in an array with different values
        """
        self.assertEqual(arrays.max(self.sample_array), 45)

    def test_max_returns_correct_value_array_with_same_values(self):
        """
        Test the max value in an array with same values
        """
        self.assertEqual(arrays.max([45,45,45,45]), 45)

    def test_max_raises_error_if_empty_array(self):
        """
        Test the max value in an array with same values
        """
        self.assertRaises(ValueError, arrays.max, [])

    def test_min_correct_value_from_array_with_different_values(self):
        """
        Test the min value in an array with different values
        """
        self.assertEqual(arrays.min(self.sample_array), 4)
    
    def test_min_correct_value_from_array_with_same_values(self):
        """
        Test the min value in an array with same values
        """
        self.assertEqual(arrays.min([2,2,2,2,2]), 2)

    def test_min_raises_error_if_empty_array(self):
        """
        Test min returns an error if array is empty
        """
        self.assertRaises(ValueError, arrays.min, [])

    def test_average_with_different_values(self):
        """
        Test the average of the values in an array with different values
        """
        self.assertEqual(arrays.average(self.sample_array), 13.666666666666666)

    def test_average_with_same_values(self):
        """
        Test the average of the values in an array with same values
        """
        self.assertEqual(arrays.average([13, 13, 13, 13, 13]), 13)

    def test_average_raises_error_if_empty_array(self):
        """
        Test average returns an error if array is empty
        """
        self.assertRaises(ValueError, arrays.average, [])
        
    def test_standard_deviation_with_different_values(self):
        """
        Test the standard deviation on the array with default values
        """
        self.assertEqual(arrays.std_dev(self.sample_array), 14.2556031868954)

    def test_standard_deviation_raises_error_on_empty_array(self):
        """
        Test the standard deviation on an empty array
        """
        self.assertRaises(ValueError, arrays.std_dev, [])

    def test_size_for_array_with_different_values(self):
        """
        Test the size for an array with different values
        """
        self.assertEqual(arrays.size(self.sample_array), 6)

    def test_size_for_an_empty_array(self):
        """
        Test the size for an empty array
        """
        self.assertEqual(arrays.size([]), 0)

    def test_sort_in_asc_order(self):
        """
        Test if the array is sorted in ascending order
        """
        sorted_array = [4,5,7,9,12,45]
        self.assertEqual(arrays.sort(self.sample_array, type='asc'), sorted_array)

    def test_sort_desc_order(self):
        """
        Test if the array is sorted in descending order
        """
        sorted_array = [45,12,9,7,5,4]
        self.assertEqual(arrays.sort(self.sample_array, type='desc'), sorted_array)

    def test_reverse(self):
        """
        Test if the array is reversed
        """
        reversed_array = [45,12,9,4,5,7]
        self.assertEqual(arrays.reverse(self.sample_array), reversed_array)


if __name__ == '__main__':
    unittest.main(verbosity=2)