# Tarefa - Construção de uma biblioteca de operações sobre arrays

Esta biblioteca deve incluir as seguintes operacoes:

retorna o maximo de todos os valores do array x;
```
float max(array x);
```

retorna o minimo de todos os valores do array x;
```
float min(array x);
```

retorna a media de todos os valores do array x;
```
float average(array x);
```

retorna o desvio padrao de todos os valores do array x;
```
float std_dev(array x);
```

retorna o tamanho do array x;
```
int size(array x);
```

retorna o array x ordenado de acordo com o tipo de ordenacao type;
asc -> ordem crescente;
desc -> ordem decrescente;
```
array sort(array x, string type);
```

retorna o reverso do array x
```
array reverse(array x);
```

## Testes
Os testes vão correr para garantir que os resultados das funções são os esperados.
* Nos arrays com valores esperados
* Nos arrays vazios (deve ser retornada uma excessão)


## Correr os testes
Para correr os testes basta executar o seguinte comando na raiz do projeto

```bash
python test_arrays.py
```